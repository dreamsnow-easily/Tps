{
  "1": {
    "TID": 1,
    "Name": "士兵",
    "Type": "Normal",
    "Class": "SOLDIER",
    "Resource": "Character/Soldier",
    "Description": "这是一个士兵",
    "InitLevel": 1,
    "Speed": 300
  },
  "2": {
    "TID": 2,
    "Name": "匪徒",
    "Type": "Normal",
    "Class": "BANDIT",
    "Resource": "Character/Bandit",
    "Description": "这是一个匪徒",
    "InitLevel": 1,
    "Speed": 300
  },
  "3": {
    "TID": 3,
    "Name": "中立者",
    "Type": "Normal",
    "Class": "CIVILIAN",
    "Resource": "Character/Civlian",
    "Description": "这是一个中立者",
    "InitLevel": 1,
    "Speed": 300
  },
  "1001": {
    "TID": 1001,
    "Name": "恶鬼",
    "Type": "Normal",
    "Class": "WARRIOR",
    "Resource": "Monster/M1001",
    "InitLevel": 1,
    "Speed": 300
  },
  "1002": {
    "TID": 1002,
    "Name": "-",
    "Type": "Normal",
    "Class": "WIZARD",
    "Resource": "Monster/M1002",
    "InitLevel": 1,
    "Speed": 300
  },
  "1003": {
    "TID": 1003,
    "Name": "-",
    "Type": "Normal",
    "Class": "ARCHER",
    "Resource": "Monster/M1003",
    "InitLevel": 1,
    "Speed": 300
  }
}