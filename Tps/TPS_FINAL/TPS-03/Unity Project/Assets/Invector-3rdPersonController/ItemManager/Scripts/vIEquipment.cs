﻿namespace TPS.ItemManager
{
    public interface vIEquipment
    {
        void OnEquip(vItem item);
        void OnUnequip(vItem item);
    }
}