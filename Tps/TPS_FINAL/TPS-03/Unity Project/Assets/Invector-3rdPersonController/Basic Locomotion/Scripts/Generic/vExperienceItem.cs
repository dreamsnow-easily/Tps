﻿using UnityEngine;
using System.Collections;
using TPS.CharacterController;

public class vExperienceItem : MonoBehaviour 
{
    [Tooltip("How much experience will be recovery")]
	public float value;
	
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag.Equals("Player"))
		{            
            // access the basic character information
			var iChar = other.GetComponent<vThirdPersonController>();
            if(iChar != null)
            {                
                // apply value plus the current health
                var targetexperience = iChar.currentExperience + value;
                // heal only if the character's health isn't full
                if (iChar.currentExperience < iChar.maxExperience)
                {
                    // limit healing to the max health
                    iChar.currentExperience = Mathf.Clamp(targetexperience, 0, iChar.maxExperience);
                    Destroy(gameObject);
                }
                else
                {
                    var tpInput = other.GetComponent<vThirdPersonInput>();
                    // show message if the character's health is full
                    tpInput.hud.ShowText("Experience is Full");                    
                }
            } 
		}
	}
}
