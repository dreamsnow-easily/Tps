﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
namespace TPS.CharacterController
{
    public class vOnDeadTrigger : MonoBehaviour
    {

        public UnityEvent OnDead;
        //public GameObject drop;

        void Start()
        {
            vCharacter character = GetComponent<vCharacter>();
            if (character)
                character.onDead.AddListener(OnDeadHandle);
           // if(this.gameObject)
           // Instantiate(drop, transform.position, Quaternion.identity);//死后产生金币
        }

        public void OnDeadHandle(GameObject target)
        {
            OnDead.Invoke();
           
        }


    }
}
