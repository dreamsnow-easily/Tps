﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TPS;

public class vAmmoListData : ScriptableObject
{
    public List<vAmmo> ammos = new List<vAmmo>();  
}
