﻿using UnityEngine;
using System.Collections;
using UnityEditor;
public partial class vMenuComponent
{
    [MenuItem("TPS/Shooter/Components/LockOn (Player Shooter Only)")]
    static void LockOnShooterMenu()
    {
        if (Selection.activeGameObject && Selection.activeGameObject.GetComponent<TPS.CharacterController.vThirdPersonInput>() != null)
            Selection.activeGameObject.AddComponent<vLockOnShooter>();
        else
            Debug.Log("Please select a Player to add the component.");
    }
}
