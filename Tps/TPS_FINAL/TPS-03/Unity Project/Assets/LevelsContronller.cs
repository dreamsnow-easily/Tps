﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelsController : MonoBehaviour {

	[Header("最高可挑战的关卡")]
	private int challengingLevel;

	/// <summary>
	/// 选择框
	/// </summary>
	private Transform select;

	private int currentLevelNum;

	void Awake()
	{
		//找到选择框
		select = GameObject.FindWithTag("Select").transform;
	}

	void Start()
	{
		challengingLevel = Singleton.GetInstance().levelAndStars.Count + 1;

		InitUI();
	}


	public void InitUI()
	{
		//初始化UI
		for (int i = 0; i < transform.childCount; i++)
		{
			//找到第i个子对象（关卡）
			Transform child = transform.GetChild(i);

			Transform stars = child.Find("stars");

			//更改子对象关卡编号
			child.Find("Text").GetComponent<Text>().text = (i+1).ToString();

			//如果当前关卡编号小于最高可挑战关卡编号，就解锁
			if (i < challengingLevel)
			{
				//把小锁图片设为非激活
				child.Find("lockImage").gameObject.SetActive(false);
				//让关卡按钮可以点击
				child.GetComponent<Button>().interactable = true;
				//将解锁后的关卡编号文字颜色变亮
				child.Find("Text").GetComponent<Text>().color = new Color32(255, 234, 149,255);
				//最新解锁的关卡就解锁以上内容直接continue
				if (i + 1 == challengingLevel)
				{
					continue;
				}
				
				//将星星背景框激活
				stars.gameObject.SetActive(true);
				//根据各个关卡获得的星星数激活指定数量的星星
				switch (Singleton.GetInstance().levelAndStars[i + 1])
				{
					case 1:
						stars.GetChild(0).gameObject.SetActive(true);
						break;

					case 2:
						stars.GetChild(0).gameObject.SetActive(true);
						stars.GetChild(2).gameObject.SetActive(true);
						break;

					case 3:
						for (int j = 0; j < 3; j++)
						{
							stars.GetChild(j).gameObject.SetActive(true);
						}
						break;

					default:
						break;
				}


			}
		}
	}

	//点击关卡按钮时响应此方法（传入的是关卡编号）
	public void CheckLevelByClick(int levelNum)
	{
		//获取当前选择的关卡
		Transform currentLevel = transform.GetChild(levelNum - 1);

		//选择框的父对象变更为当前选中的关卡
		select.SetParent(currentLevel);

		//选择框放到最底层
		select.SetSiblingIndex(0);

		//选择框位置变更到当前选择的关卡
		select.localPosition = Vector3.zero;

		//保存当前选中的关卡编号（这里也可以直接用单例对象接收）
		currentLevelNum = levelNum;
	}

	//点击对号按钮时执行此方法
	public void OnClickLoadScene()
	{
		//将当前关卡编号保存在单例中
		Singleton.GetInstance().levelNum = currentLevelNum;
		//跳转场景
		SceneManager.LoadScene("Complete");
	}

}

