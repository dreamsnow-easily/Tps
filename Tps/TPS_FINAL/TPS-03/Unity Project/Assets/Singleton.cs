﻿using System.Collections.Generic;

public class Singleton
{

    //当前进入的关卡
    public int levelNum;

    //当前关卡中获得的星星数量
    public int getStars;

    //定义字典用来存储所有关卡信息
    public Dictionary<int, int> levelAndStars;
    //定义单例对象
    private static Singleton instance;
    //在构造中初始化字典
    private Singleton()
    {
        levelAndStars = new Dictionary<int, int>();
    }
    //获取单例
    public static Singleton GetInstance()
    {
        if (instance == null)
        {
            instance = new Singleton();
        }
        return instance;
    }
    //更新关卡信息方法（在complete场景中调用）
    public void UpdateLevelStars()
    {
        if (levelAndStars.ContainsKey(levelNum))
        {
            if (levelAndStars[levelNum] < getStars)
            {
                levelAndStars[levelNum] = getStars;
            }
        }
        else
        {
            levelAndStars.Add(levelNum, getStars);
        }


    }

}
